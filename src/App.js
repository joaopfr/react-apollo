import React, { Component } from 'react';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';
import RecipesContainer from './RecipesContainer';

const uri = 'http://localhost:4000/'
const client = new ApolloClient({
  uri: uri
});

class App extends Component {
  render() {
    return (
      <ApolloProvider client={client}>
        <RecipesContainer/>
      </ApolloProvider>
    );
  }
}

export default App;
