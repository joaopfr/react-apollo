import gql from 'graphql-tag';
import { graphql } from 'react-apollo';
import React from 'react';
import VegetarianCheck from './VegetarianCheck';
import RecipesView from './RecipesView';

const query = gql`
  query recipes($vegetarian: Boolean!) {
    recipes(vegetarian: $vegetarian) {
      id
      title
    }
  }
`;

class RecipesContainer extends React.Component {
    constructor(props) {
        super(props);
        const isVegetarian = false;

        this.state = { isVegetarian };
        this.handleOnChange = this.handleOnChange.bind(this);
    }

    handleOnChange({ target: { checked } }) {
        const isVegetarian = checked;
        this.setState({ isVegetarian });
    }

    render() {
        const vegetarian = this.state.isVegetarian;
        const RecipesQuery = graphql(
            query,
            { options: { variables: { vegetarian } } }
        )(RecipesView);

        return (
            <React.Fragment>
                <VegetarianCheck
                    isVegetarian={this.state.isVegetarian}
                    handleOnChange={this.handleOnChange}
                />
                <RecipesQuery/>
            </React.Fragment>
        );
    }
}

export default RecipesContainer;