import React from 'react';

const Recipes = ({ recipes }) => (
    <ul>
        {
            recipes.map(
                ({ id, title }) => 
                    <li key={id}>{title}</li>
            )
        }
    </ul>
);

export default Recipes;