import React from 'react';

const VegetarianCheck = ({ isVegetarian, handleOnChange }) => (
    <div>
        <label>
            <input type='checkbox' onChange={handleOnChange} checked={isVegetarian}/>
            Only Vegetarian
        </label>
    </div>
);

export default VegetarianCheck;