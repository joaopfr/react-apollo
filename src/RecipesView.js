import Recipes from "./Recipes";
import React from 'react';


const RecipesView = ({ data: { recipes, loading: isLoading, error } }) => {
  if (isLoading) return <p>Loading...</p>;
  if (error) return <p>Something went wrong</p>;
  return <Recipes recipes={recipes}/>;
};

export default RecipesView;